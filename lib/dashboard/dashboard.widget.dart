import 'package:discovery_bank/app/model/app.model.dart';
import 'package:discovery_bank/app/navigation/app.routes.dart';
import 'package:discovery_bank/theme/text-styles.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardWidget createState() {
    return _DashboardWidget();
  }
}

class _DashboardWidget extends State<Dashboard> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final AppModel app = Provider.of<AppModel>(context, listen: false);

    Future.delayed(Duration.zero, () {
      if (app.currentRoute == AppRoutes.DASHBOARD) {
        app.setPageTitle('Discovery Bank');
      }
    });

    return Container(
      alignment: Alignment.center,
      color: Colors.purple,
      child: Text(
        'Dashboard',
        style: TextStyles.HEADER_1.merge(TextStyle(color: Colors.white)),
      ),
    );
  }
}
