import 'package:discovery_bank/core/gradient-line.widget.dart';
import 'package:discovery_bank/theme/gradients.dart';
import 'package:discovery_bank/theme/text-styles.dart';
import 'package:flutter/material.dart';

class CallOut extends StatelessWidget {
  final String text;

  const CallOut(this.text);

  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.symmetric(vertical: 24, horizontal: 0),
      child: IntrinsicHeight(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            GradientLine(
                width: 2,
                height: double.infinity,
                gradient: Gradients.DISCOVERY_VERTICAL),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(top: 14, bottom: 14, left: 14),
                child: Text(
                  text,
                  textAlign: TextAlign.left,
                  style: TextStyles.PARAGRAPH.merge(TextStyle(color: Colors.grey[700])),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
