import 'package:discovery_bank/app/model/app.model.dart';
import 'package:discovery_bank/theme/text-styles.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Transfers extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final AppModel app = Provider.of<AppModel>(context, listen: false);

    Future.delayed(Duration.zero, () {
      app.setPageTitle('Transfers');
    });

    return Container(
      alignment: Alignment.center,
      color: Colors.blue,
      child: Text(
        'Transfer',
        style: TextStyles.HEADER_1.merge(TextStyle(color: Colors.white)),
      ),
    );
  }
}
