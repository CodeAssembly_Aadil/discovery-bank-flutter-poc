import 'package:flutter/material.dart';

class DiscoveryPayModel extends ChangeNotifier {
  bool _splashScreenVisible = false;
  bool _receivePayments = false;

  bool get splashScreenVisible => _splashScreenVisible;
  bool get receivePayments => _receivePayments;

  void optIn() {
    _receivePayments = true;
    notifyListeners();
  }

  void optOut() {
    _receivePayments = false;
    notifyListeners();
  }

  void showSplashScreen() {
    _splashScreenVisible = true;
    notifyListeners();
  }

  void hideSplashScreen() {
    _splashScreenVisible = false;
    notifyListeners();
  }
}
