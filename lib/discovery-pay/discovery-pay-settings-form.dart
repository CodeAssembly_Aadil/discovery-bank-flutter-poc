import 'package:discovery_bank/discovery-pay/discovery-pay-setup.dart';
import 'package:discovery_bank/theme/text-styles.dart';
import 'package:flutter/material.dart';

class DiscoveryPaySettingsForm extends StatefulWidget {
  bool showSetUpSplash;

  @override
  _DiscoveryPaySettingsForm createState() => _DiscoveryPaySettingsForm();
}

class _DiscoveryPaySettingsForm extends State<DiscoveryPaySettingsForm> {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topCenter,
      padding: const EdgeInsets.all(24.0),
      decoration: BoxDecoration(color: Colors.white),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(
              // crossAxisAlignment: CrossAxisAlignment.s,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                    child: Text(
                  'Receive Discovery Pay Payments',
                  textAlign: TextAlign.left,
                  // overflow: Text,
                  style: TextStyles.HEADER_5.merge(TextStyle(
                      color: Colors.grey[900], fontWeight: FontWeight.w400)),
                )),
                Switch(
                  value: false,
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.only(top: 14, bottom: 24),
              child: Text(
                'This will allow Discovery Bank clients to locate and pay you using Discovery Pay.',
                textAlign: TextAlign.left,
                style: TextStyles.PARAGRAPH,
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 24),
              child: Text(
                'Welcome to Discovery Pay! Setup your profile to receive payments from via your cellphone number from registered Discovery Pay users.',
                textAlign: TextAlign.center,
                style: TextStyles.PARAGRAPH,
              ),
            ),
            FlatButton(
              color: Colors.blue[700],
              textColor: Colors.white,
              disabledColor: Colors.grey,
              disabledTextColor: Colors.black,
              padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 40.0),
              splashColor: Colors.blueAccent,
              onPressed: () {
                /*...*/
              },
              child: Text(
                "Setup",
                style: TextStyle(fontSize: 14.0),
              ),
            ),
          ]),
    );
  }
}
