import 'package:discovery_bank/app/model/app.model.dart';
import 'package:discovery_bank/discovery-pay/discovery-pay-settings.dart';
import 'package:discovery_bank/discovery-pay/discovery-pay-setup.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DiscoveryPay extends StatefulWidget {
  @override
  _DiscoveryPay createState() {
    return _DiscoveryPay();
  }
}

class _DiscoveryPay extends State<DiscoveryPay> {
  bool _showSplash = true;
  bool _optIntoDiscoveryPay = false;

  @override
  Widget build(BuildContext context) {
    final AppModel app = Provider.of<AppModel>(context, listen: false);

    Future.delayed(Duration.zero, () {
      app.setPageTitle('Discovery Pay');
    });

    return Container(
      alignment: Alignment.center,
      // color: Colors.teal,
      child: _showSplash
          ? DiscoveryPaySetup(hideSplashScreen)
          : DiscoveryPaySettings(),
    );
  }

  void hideSplashScreen() {
    setState(() => this._showSplash = false);
  }
}
