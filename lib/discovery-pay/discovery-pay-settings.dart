import 'package:discovery_bank/common/callout.dart';
import 'package:discovery_bank/core/gradient-line.widget.dart';
import 'package:discovery_bank/discovery-pay/discovery-pay-setup.dart';
import 'package:discovery_bank/theme/text-styles.dart';
import 'package:flutter/material.dart';

class DiscoveryPaySettings extends StatefulWidget {
  bool showSetUpSplash;

  @override
  _DiscoveryPaySettings createState() => _DiscoveryPaySettings();
}

class _DiscoveryPaySettings extends State<DiscoveryPaySettings> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(24.0),
      decoration: BoxDecoration(color: Colors.white),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                    child: Text(
                  'Receive Discovery Pay Payments',
                  textAlign: TextAlign.left,
                  // overflow: Text,
                  style: TextStyles.HEADER_5.merge(TextStyle(
                      color: Colors.grey[900], fontWeight: FontWeight.w400)),
                )),
                Switch(
                  value: false,
                ),
              ],
            ),
            CallOut( 'This will allow Discovery Bank clients to locate and pay you using Discovery Pay.'),
            // Container(
            //   margin: EdgeInsets.only(top: 14, bottom: 24),
            //   child: Text(
            //     'This will allow Discovery Bank clients to locate and pay you using Discovery Pay.',
            //     textAlign: TextAlign.left,
            //     style: TextStyles.PARAGRAPH,
            //   ),
            // ),
          ]),
    );
  }
}
