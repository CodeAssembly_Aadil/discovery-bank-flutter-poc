import 'package:discovery_bank/theme/text-styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class DiscoveryPaySetup extends StatelessWidget {
  final Function onSetup;

  const DiscoveryPaySetup(this.onSetup);

  @override
  Widget build(BuildContext context) {
    return Container(
      // alignment: Alignment.center,
      padding: const EdgeInsets.all(32.0),
      decoration: BoxDecoration(color: Colors.white),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: 40),
              child: SvgPicture.asset('assets/images/pay_icon.svg',
                  semanticsLabel: 'Discovery Pay Icon'),
            ),
            Container(
                margin: EdgeInsets.only(bottom: 24),
                child: Text(
                  'Setup Discovery Pay',
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyles.HEADER_1,
                )),
            Container(
              margin: EdgeInsets.only(bottom: 24),
              child: Text(
                'Welcome to Discovery Pay! Setup your profile to receive payments from via your cellphone number from registered Discovery Pay users.',
                textAlign: TextAlign.center,
                style: TextStyles.PARAGRAPH,
              ),
            ),
            FlatButton(
              color: Colors.blue[700],
              textColor: Colors.white,
              disabledColor: Colors.grey,
              disabledTextColor: Colors.black,
              padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 40.0),
              splashColor: Colors.blueAccent,
              onPressed: () {
                if (onSetup != null) {
                  onSetup();
                }
              },
              child: Text(
                "Setup",
                style: TextStyle(fontSize: 14.0),
              ),
            ),
          ]),
    );
  }
}
