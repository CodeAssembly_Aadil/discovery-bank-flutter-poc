import 'package:discovery_bank/app/model/app.model.dart';
import 'package:discovery_bank/theme/text-styles.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Pay extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final AppModel app = Provider.of<AppModel>(context, listen: false);

    Future.delayed(Duration.zero, () {
      app.setPageTitle('Pay');
    });

    return Container(
      alignment: Alignment.center,
      color: Colors.red,
      child: Text(
        'Pay',
        style: TextStyles.HEADER_1.merge(TextStyle(color: Colors.white)),
      ),
    );
  }
}
