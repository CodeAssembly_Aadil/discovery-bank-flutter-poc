import 'package:discovery_bank/app/discovery-bank.app.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(DiscoveryBankApp());
