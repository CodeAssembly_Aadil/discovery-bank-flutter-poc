import 'package:discovery_bank/theme/gradients.dart';
import 'package:flutter/material.dart';

class GradientShader extends StatelessWidget {
  final Widget child;
  final Gradient gradient;

  const GradientShader(
      {@required this.child, this.gradient = Gradients.DISCOVERY});

  @override
  Widget build(BuildContext context) {
    return ShaderMask(
      shaderCallback: (Rect bounds) {
        return gradient.createShader(Offset.zero & bounds.size);
      },
      child: child,
    );
  }
}