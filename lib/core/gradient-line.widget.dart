import 'package:discovery_bank/theme/gradients.dart';
import 'package:flutter/material.dart';

class GradientLine extends StatelessWidget {
  final double width, height;
  final Gradient gradient;

  const GradientLine(
      {this.width = double.infinity,
      this.height = 1,
      this.gradient = Gradients.DISCOVERY});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        gradient: gradient,
      ),
    );
  }
}
