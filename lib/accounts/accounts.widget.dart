import 'package:discovery_bank/app/model/app.model.dart';
import 'package:discovery_bank/theme/text-styles.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Accounts extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final AppModel app = Provider.of<AppModel>(context, listen: false);
    
    Future.delayed(Duration.zero, () {
      app.setPageTitle('Accounts');
    });

    return Container(
      alignment: Alignment.center,
      color: Colors.green,
      child: Text(
        'Accounts',
        style: TextStyles.HEADER_1.merge(TextStyle(color: Colors.white)),
      ),
    );
  }
}
