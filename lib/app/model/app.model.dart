import 'package:discovery_bank/app/navigation/app.routes.dart';
import 'package:flutter/material.dart';

class AppModel extends ChangeNotifier {
  String _currentRoute = AppRoutes.DASHBOARD;
  String _pageTitle = 'Discovery Bank';

  String get currentRoute => _currentRoute;
  String get pageTitle => _pageTitle;

  void setPageTitle(String title) {
    if (_pageTitle != title) {
      _pageTitle = title;
      notifyListeners();
    }
  }

  void setCurrentRoute(String route) {
    if (_currentRoute != route) {
      _currentRoute = route;
      notifyListeners();
    }
  }
}
