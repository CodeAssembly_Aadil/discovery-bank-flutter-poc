import 'package:discovery_bank/theme/gradients.dart';
import 'package:flutter/material.dart';

class NavigationTabItemWidget extends StatelessWidget {
  static const _inactiveColor = const Color(0xFFBDBDBD);

  static const LinearGradient _inactiveShader =
      const LinearGradient(colors: <Color>[_inactiveColor, _inactiveColor]);

  final Widget item;
  final bool isActive;

  const NavigationTabItemWidget({this.item, this.isActive});

  @override
  Widget build(BuildContext context) {
    return ShaderMask(
      shaderCallback: (Rect bounds) {
        return isActive
            ? Gradients.DISCOVERY.createShader(Offset.zero & bounds.size)
            : _inactiveShader.createShader(Offset.zero & bounds.size);
      },
      child: item,
    );
  }
}
