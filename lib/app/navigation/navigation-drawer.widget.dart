import 'package:discovery_bank/app/navigation/app.navigator.dart';
import 'package:discovery_bank/app/navigation/navigation-drawer.routes.dart';
import 'package:discovery_bank/core/gradient-shader.dart';
import 'package:flutter/material.dart';

final GlobalKey<NavigatorState> APP_NAV_DRAWER = GlobalKey();

class NavigationDrawer extends StatelessWidget {
  final String currentRoute;

  NavigationDrawer({this.currentRoute});

  List<Widget> buildListTiles(BuildContext context) {
    print('currentRoute $currentRoute');
    List<ListTile> items = [];

    for (int i = 0, l = NAVIGATION_DRAWER_ROUTES.length; i < l; i++) {
      String route = NAVIGATION_DRAWER_ROUTES[i];
      String title = NAVIGATION_DRAWER_ROUTE_TITLES[i];

      print(
          'currentRoute == route: $currentRoute $route ${currentRoute == route}');

      items.add(ListTile(
        title: currentRoute == route
            ? GradientShader(child: Text(title))
            : GradientShader(child: Text(title)),
        onTap: () {
          Navigator.pop(context);
          APP_NAVIGATOR.currentState.popAndPushNamed(route);
        },
      ));
    }

    return items;
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      key: APP_NAV_DRAWER,
      child: SafeArea(
        child: ListView(
          padding: EdgeInsets.zero,
          children: buildListTiles(context),
        ),
      ),
    );
  }
}
