import 'package:discovery_bank/accounts/accounts.widget.dart';
import 'package:discovery_bank/app/model/app.model.dart';
import 'package:discovery_bank/app/navigation/app.routes.dart';
import 'package:discovery_bank/dashboard/dashboard.widget.dart';
import 'package:discovery_bank/discovery-pay/discovery-pay.widget.dart';
import 'package:discovery_bank/pay/pay.widget.dart';
import 'package:discovery_bank/transfers/transfers.widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

final GlobalKey<NavigatorState> APP_NAVIGATOR = GlobalKey();

class AppNavigator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final AppModel app = Provider.of<AppModel>(context, listen: false);

    return Navigator(
      key: APP_NAVIGATOR,
      initialRoute: AppRoutes.DASHBOARD,
      //
      onGenerateRoute: (RouteSettings settings) {
        WidgetBuilder builder;

        print('AppNavigator::Navigator:onGenerateRoute ${settings.name}');

        switch (settings.name) {
          case AppRoutes.ROOT:
          case AppRoutes.DASHBOARD:
            builder = (BuildContext b) => Dashboard();
            break;
          case AppRoutes.ACCOUNTS:
            builder = (BuildContext b) => Accounts();
            break;
          case AppRoutes.PAY:
            builder = (BuildContext b) => Pay();
            break;
          case AppRoutes.TRANSFERS:
            builder = (BuildContext b) => Transfers();
            break;
          case AppRoutes.DISCOVERY_PAY:
            builder = (BuildContext b) => DiscoveryPay();
            break;
          default:
            throw Exception('Invalid route: ${settings.name}');
        }

        Future.delayed(Duration.zero, () {
          app.setCurrentRoute(settings.name);
        });

        return MaterialPageRoute(builder: builder, settings: settings);
      },
      
    );
  }
}
