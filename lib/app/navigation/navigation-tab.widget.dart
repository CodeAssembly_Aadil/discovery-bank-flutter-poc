import 'package:discovery_bank/app/navigation/app.navigator.dart';
import 'package:discovery_bank/app/navigation/app.routes.dart';
import 'package:discovery_bank/app/navigation/navigation-tab.routes.dart';
import 'package:discovery_bank/core/gradient-line.widget.dart';
import 'package:discovery_bank/core/gradient-shader.dart';
import 'package:flutter/material.dart';

class NavigationTabWidget extends StatelessWidget {
  static const SELECTED_TEXT_STYLE =
      const TextStyle(color: Colors.white, fontSize: 12);

  static const SELECTED_ICON_THEME_DATA =
      const IconThemeData(color: Colors.white, size: 24);

  static const UNSELECTED_TEXT_STYLE =
      const TextStyle(color: Color(0xFFBDBDBD), fontSize: 12);

  static const UNSELECTED_ICON_THEME_DATA =
      const IconThemeData(color: Color(0xFFBDBDBD), size: 24);

  static const List<BottomNavigationBarItem> _NAVIGATION_ITEMS =
      const <BottomNavigationBarItem>[
    BottomNavigationBarItem(
      icon: Icon(Icons.home),
      title: Text('Home'),
    ),
    BottomNavigationBarItem(
      icon: Icon(Icons.filter_none),
      title: Text('Accounts'),
    ),
    BottomNavigationBarItem(
      icon: Icon(Icons.add_circle_outline),
      title: Text('Pay'),
    ),
    BottomNavigationBarItem(
      icon: Icon(Icons.swap_horiz),
      title: Text('Transfer'),
    ),
    BottomNavigationBarItem(
      icon: Icon(Icons.more_horiz),
      title: Text('More'),
    ),
  ];

  final String currentRoute;
  final Function onShowMore;

  const NavigationTabWidget({this.currentRoute, @required this.onShowMore});

  int _mapRouteToIndex(String route) {
    int routeIndex = NAVIGATION_TAB_ROUTES.indexOf(route);

    if (routeIndex != -1) {
      return routeIndex;
    }
    // default to more
    return 4; //route.startsWith(AppRoutes.MORE) ? 4 : null;
  }

  void _navigateToRoute(int index) {
    if (index == NAVIGATION_TAB_ROUTES.indexOf(AppRoutes.MORE) &&
        onShowMore != null) {
      onShowMore();
    } else if (0 <= index && index < NAVIGATION_TAB_ROUTES.length) {
      APP_NAVIGATOR.currentState.popAndPushNamed(NAVIGATION_TAB_ROUTES[index]);
    }
  }

  List<BottomNavigationBarItem> _buildNavigationItems() {
    int activeIndex = _mapRouteToIndex(currentRoute);

    List<BottomNavigationBarItem> items = List.from(_NAVIGATION_ITEMS);

    BottomNavigationBarItem activeItem = items[activeIndex];

    items[activeIndex] = BottomNavigationBarItem(
        icon: GradientShader(child: activeItem.icon),
        title: GradientShader(child: activeItem.title));

    return items;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        GradientLine(),
        BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          backgroundColor: Colors.white,

          // Events
          currentIndex: _mapRouteToIndex(currentRoute),
          onTap: _navigateToRoute,

          // Unselected
          selectedIconTheme: SELECTED_ICON_THEME_DATA,
          showUnselectedLabels: true,
          selectedLabelStyle: SELECTED_TEXT_STYLE,

          // Selected
          unselectedIconTheme: UNSELECTED_ICON_THEME_DATA,
          showSelectedLabels: true,
          unselectedLabelStyle: UNSELECTED_TEXT_STYLE,

          items: _buildNavigationItems(),
        ),
      ],
    );
  }
}
