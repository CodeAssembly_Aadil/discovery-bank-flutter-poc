class AppRoutes {
  static const ROOT = '/';

  // Bottom Tab Routes
  static const DASHBOARD = '/Dashboard';
  static const ACCOUNTS = '/Accounts';
  static const TRANSFERS = '/Transfers';
  static const PAY = '/Pay';
  static const MORE = '/More';
  // More Menu
  static const DISCOVERY_PAY = MORE + '/Discovery_Pay';
  static const DYNAMIC_CREDIT_FACILITY = MORE + '/Dynamic_Credit_Facility';
}
