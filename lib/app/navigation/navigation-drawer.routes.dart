import 'package:discovery_bank/app/navigation/app.routes.dart';

const NAVIGATION_DRAWER_ROUTES = const <String>[
  AppRoutes.DISCOVERY_PAY,
  AppRoutes.DYNAMIC_CREDIT_FACILITY,
];

const NAVIGATION_DRAWER_ROUTE_TITLES = const <String>[
  'Discovery Pay',
  'Dynamic Credit Facility',
];
