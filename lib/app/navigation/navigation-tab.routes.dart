import 'package:discovery_bank/app/navigation/app.routes.dart';

const NAVIGATION_TAB_ROUTES = const <String>[
  AppRoutes.DASHBOARD,
  AppRoutes.ACCOUNTS,
  AppRoutes.PAY,
  AppRoutes.TRANSFERS,
  AppRoutes.MORE
];
