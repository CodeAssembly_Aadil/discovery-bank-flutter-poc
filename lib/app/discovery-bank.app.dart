import 'package:discovery_bank/app/model/app.model.dart';
import 'package:discovery_bank/app/navigation/app.navigator.dart';
import 'navigation/navigation-drawer.widget.dart';
import 'package:discovery_bank/app/navigation/navigation-tab.widget.dart';
import 'package:discovery_bank/core/gradient-line.widget.dart';
import 'package:discovery_bank/theme/themes.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

final GlobalKey<ScaffoldState> APP_SCAFFOLD = GlobalKey();

class DiscoveryBankApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Discovery Bank',
      theme: Themes.DISCOVERY_THEME,
      home: ChangeNotifierProvider(
        builder: (context) => AppModel(),
        child: Scaffold(
          key: APP_SCAFFOLD,
          //
          appBar: AppBar(
            title: Consumer<AppModel>(
              builder: (BuildContext context, AppModel app, Widget child) =>
                  Text(
                app.pageTitle,
                style: TextStyle(color: Colors.grey[900]),
              ),
            ),
            backgroundColor: Colors.white,
            elevation: 0,
            flexibleSpace: Container(
              alignment: Alignment.bottomCenter,
              child: GradientLine(),
            ),
          ),
          //
          body: AppNavigator(),
          //
          endDrawer: Consumer<AppModel>(
            builder: (BuildContext context, AppModel app, Widget child) =>
                NavigationDrawer(currentRoute: app.currentRoute),
          ),
          //
          bottomNavigationBar: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              GradientLine(),
              Consumer<AppModel>(
                builder: (BuildContext context, AppModel app, Widget child) =>
                    NavigationTabWidget(
                        currentRoute: app.currentRoute,
                        onShowMore: () {
                          if (APP_SCAFFOLD.currentState.isEndDrawerOpen) {
                            Navigator.pop(context);
                          } else {
                            APP_SCAFFOLD.currentState.openEndDrawer();
                          }
                        }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
