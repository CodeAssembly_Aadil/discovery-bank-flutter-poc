import 'package:flutter/material.dart';

class Themes {
  static final ThemeData DISCOVERY_THEME = ThemeData(primaryColor: Colors.blue[500], fontFamily: 'OpenSans');
}
