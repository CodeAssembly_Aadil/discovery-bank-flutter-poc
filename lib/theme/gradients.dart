import 'package:flutter/material.dart';

class Gradients {
  static const DISCOVERY = const LinearGradient(
    begin: Alignment.centerLeft,
    end: Alignment.centerRight,
    colors: [
      const Color.fromRGBO(30, 225, 203, 1),
      const Color.fromRGBO(99, 33, 254, 1),
      const Color.fromRGBO(255, 0, 153, 1),
    ],
    stops: [0.0, 0.5, 1.0],
    tileMode: TileMode.clamp,
  );

  static const DISCOVERY_VERTICAL = const LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [
      const Color.fromRGBO(30, 225, 203, 1),
      const Color.fromRGBO(99, 33, 254, 1),
      const Color.fromRGBO(255, 0, 153, 1),
    ],
    stops: [0.0, 0.5, 1.0],
    tileMode: TileMode.clamp,
  );

  static const VITALITY = const LinearGradient(
    begin: Alignment.centerLeft,
    end: Alignment.centerRight,
    colors: [
      const Color.fromRGBO(253, 135, 19, 1),
      const Color.fromRGBO(255, 90, 34, 1),
      const Color.fromRGBO(237, 67, 27, 1),
    ],
    stops: [0.0, 0.5, 1.0],
    tileMode: TileMode.clamp,
  );

  static const BLUE = const LinearGradient(
    begin: Alignment.centerLeft,
    end: Alignment.centerRight,
    colors: [
      const Color.fromRGBO(63, 112, 173, 1),
      const Color.fromRGBO(158, 203, 250, 1),
    ],
    stops: [0.0, 1.0],
    tileMode: TileMode.clamp,
  );

  static const BRONZE = const LinearGradient(
    begin: Alignment.centerLeft,
    end: Alignment.centerRight,
    colors: [
      const Color.fromRGBO(168, 101, 69, 1),
      const Color.fromRGBO(241, 157, 91, 1),
    ],
    stops: [0.0, 1.0],
    tileMode: TileMode.clamp,
  );

  static const SILVER = const LinearGradient(
    begin: Alignment.centerLeft,
    end: Alignment.centerRight,
    colors: [
      const Color.fromRGBO(130, 130, 130, 1),
      const Color.fromRGBO(224, 224, 224, 1),
    ],
    stops: [0.0, 1.0],
    tileMode: TileMode.clamp,
  );

  static const GOLD = const LinearGradient(
    begin: Alignment.centerLeft,
    end: Alignment.centerRight,
    colors: [
      const Color.fromRGBO(184, 142, 66, 1),
      const Color.fromRGBO(244, 217, 101, 1)
    ],
    stops: [0.0, 1.0],
    tileMode: TileMode.clamp,
  );

  static const DIAMOND = const LinearGradient(
    begin: Alignment.centerLeft,
    end: Alignment.centerRight,
    colors: [
      const Color.fromRGBO(183, 195, 214, 1),
      const Color.fromRGBO(223, 208, 224, 1),
      const Color.fromRGBO(197, 175, 201, 1),
    ],
    stops: [0.0, 0.5, 1.0],
    tileMode: TileMode.clamp,
  );
}
