import 'package:flutter/material.dart' show Colors;
import 'package:flutter/painting.dart' show FontWeight, TextStyle;

class TextStyles {
  static const _EM = 14.0;

  static const HEADER_1 = const TextStyle(
      fontFamily: 'Open Sans',
      fontSize: _EM * 2.5,
      fontWeight: FontWeight.w700,
      color: Colors.grey);

  static const HEADER_2 = const TextStyle(
      fontFamily: 'Open Sans',
      fontSize: _EM * 2,
      fontWeight: FontWeight.w700,
      color: Colors.grey);

  static const HEADER_3 = const TextStyle(
      fontFamily: 'Open Sans',
      fontSize: _EM * 1.75,
      fontWeight: FontWeight.w700,
      color: Colors.grey);

  static const HEADER_4 = const TextStyle(
      fontFamily: 'Open Sans',
      fontSize: _EM * 1.5,
      fontWeight: FontWeight.w700,
      color: Colors.grey);

  static const HEADER_5 = const TextStyle(
      fontFamily: 'Open Sans',
      fontSize: _EM * 1.25,
      fontWeight: FontWeight.w700,
      color: Colors.grey);

  static const HEADER_6 = const TextStyle(
      fontFamily: 'Open Sans',
      fontSize: _EM * 1,
      fontWeight: FontWeight.w700,
      color: Colors.grey);

  static const PARAGRAPH = const TextStyle(
      fontFamily: 'Open Sans',
      fontSize: _EM,
      fontWeight: FontWeight.w400,
      color: Colors.grey);
}
