import 'package:flutter/material.dart';

class PrimaryColors {
  static const BLUE = const Color.fromRGBO(17, 75, 138, 1);
  static const GOLD = const Color.fromRGBO(83, 138, 97, 1);
  static const DARK_GRAY = const Color.fromRGBO(41, 43, 44, 1);
  static const GRAY = const Color.fromRGBO(115, 115, 115, 1);
  static const LIGHT_GRAY = const Color.fromRGBO(235, 235, 235, 1);
  static const WHITE = const Color.fromRGBO(41, 43, 44, 1);
  static const VITALITY_ORANGE = const Color.fromRGBO(255, 90, 34, 1);
  static const SUCCESS_GREEN = const Color.fromRGBO(0, 152, 76, 1);
}

class SecondaryColors {
  static const ALERT_YELLOW = const Color.fromRGBO(253, 184, 19, 1);
  static const CALL_OUT_RED = const Color.fromRGBO(237, 27, 46, 1);
  static const LIGHT_BLUE = const Color.fromRGBO(117, 190, 233, 1);
  static const MEDIUM_BLUE = const Color.fromRGBO(17, 138, 203, 1);
  static const BLUE_GREY = const Color.fromRGBO(148, 167, 184, 1);
  static const TURQUOISE = const Color.fromRGBO(65, 154, 164, 1);
  static const SOFT_GREEN = const Color.fromRGBO(118, 176, 66, 1);
  static const DARK_GREEN = const Color.fromRGBO(0, 144, 101, 1);
  static const ORANGE = const Color.fromRGBO(0241, 90, 34, 1);
  static const DEEP_PURPLE = const Color.fromRGBO(90, 82, 123, 1);
  static const MAGENTA = const Color.fromRGBO(140, 36, 115, 1);
}
